# Was ist mir gut gelungen?
Die Zusammenarbeit am Projekt ist okay gegangen, gleichzeitig am gleichen Projekt zu arbeiten ging deutlich einfacher als zuvor gedacht. Jedoch war ausser mir nur noch Samuel mit Git vertraut, was dazu führte, dass er und ich für den grossteil der Entwicklung zuständig waren (ca. 36/39 Commits).

Der Entwicklungsprozess ging ziemlich schnell (hat jedoch trotzdem im Kontext gesehen viel Zeit beansprucht), da wir uns auf das absolute Minimum an Funktionalität beschränkt haben.

# Wo hatte ich Schwierigkeiten
Die Zeit was viel zu knapp, ein komplette Spiel entwickeln benötigt deutlich mehr Zeit, insbesondere, wenn man auch ein gutes Ergebniss möchte.

Andere Schwierigkeiten gab es auch bei der Umsetzung des Spieles, zum Beispiel war ich für den Grossteil der Projektes verantwortlich, da ich der einzige war, der zumindest ein wenig Erfahrung mit Game-Developement und/oder Unity hat.

# Was würde ich nächstes mal anders machen?
Auf Grund des Zeitmangels, würde ich micht nächstes mal auf ein deutlich simpleres Spiel fokusieren, bei dem keine Kreativität und fast keine Entwicklung notwendig ist. Beispielsweise *Flappy Bird* oder vergleichbares.

# Was habe ich dabei gelernt?
Mindestens dreimal so viel Zeit einrechnen und falls das nicht möglich ist, dann versuchen alles aus dem Asset Store zusammenzustückeln.

Qualität ist gut, aber unter dieser Zeitlichen beschränkung nicht nöglich.

Der Einzige im Team zu sein, der halbwegs versteht wie man mit Unity entwickelt ist schwer und macht, dass man keine Zeit hat jemandem etwas beizubringen.

Zusammenarbeit ist nur dann effizient möglich, wenn alle Git beherschen, oder zumindest wissen wie man grundsätzlich damit arbeitet.